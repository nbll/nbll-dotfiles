# Autogenerated config.py # Documentation:
#   qute://help/configuring.html
#   qute://help/settings.html

# from os import system
# from time import strftime, localtime

# # Function to download YouTube video with upload date in filename
# def download_youtube_with_date(url):
#     # Get the current time
#     current_time = strftime("%Y-%m-%d_%H-%M-%S", localtime())

#     # Use youtube-dl command to download the video with upload date in the filename
#     system(f'youtube-dl -o "~/Downloads/%(upload_date)s_{current_time}.%(ext)s" {url}')
#     system(f'yt-dlp -f 135+139 -o "~/vids/qutebrowser/{current_time}_%(title)s-%(id)s.%(ext)s" {url}')
#     # config.bind('zn', 'hint links spawn alacritty -e yt-dlp -f 135+139 -o "~/vids/qutebrowser/$(date +"%Y-%m-%d_%H-%M-%S")_%(title)s-%(id)s.%(ext)s" {hint-url}')

# # Create a Qutebrowser binding for the custom command
# # config.bind('<Ctrl-Shift-d>', 'hint links spawn --detach download_youtube_with_date {hint-url}')



# Uncomment this to still load settings configured via autoconfig.yml
config.load_autoconfig(False)

# User agent to send.  The following placeholders are defined:  *
# `{os_info}`: Something like "X11; Linux x86_64". * `{webkit_version}`:
# The underlying WebKit version (set to a fixed value   with
# QtWebEngine). * `{qt_key}`: "Qt" for QtWebKit, "QtWebEngine" for
# QtWebEngine. * `{qt_version}`: The underlying Qt version. *
# `{upstream_browser_key}`: "Version" for QtWebKit, "Chrome" for
# QtWebEngine. * `{upstream_browser_version}`: The corresponding
# Safari/Chrome version. * `{qutebrowser_version}`: The currently
# running qutebrowser version.  The default value is equal to the
# unchanged user agent of QtWebKit/QtWebEngine.  Note that the value
# read from JavaScript is always the global value.
# Type: FormatString
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version}', 'https://web.whatsapp.com/')

# Setting dark mode
config.set("colors.webpage.darkmode.enabled", True)

# User agent to send.  The following placeholders are defined:  *
# `{os_info}`: Something like "X11; Linux x86_64". * `{webkit_version}`:
# The underlying WebKit version (set to a fixed value   with
# QtWebEngine). * `{qt_key}`: "Qt" for QtWebKit, "QtWebEngine" for
# QtWebEngine. * `{qt_version}`: The underlying Qt version. *
# `{upstream_browser_key}`: "Version" for QtWebKit, "Chrome" for
# QtWebEngine. * `{upstream_browser_version}`: The corresponding
# Safari/Chrome version. * `{qutebrowser_version}`: The currently
# running qutebrowser version.  The default value is equal to the
# unchanged user agent of QtWebKit/QtWebEngine.  Note that the value
# read from JavaScript is always the global value.
# Type: FormatString
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}; rv:71.0) Gecko/20100101 Firefox/71.0', 'https://accounts.google.com/*')

# User agent to send.  The following placeholders are defined:  *
# `{os_info}`: Something like "X11; Linux x86_64". * `{webkit_version}`:
# The underlying WebKit version (set to a fixed value   with
# QtWebEngine). * `{qt_key}`: "Qt" for QtWebKit, "QtWebEngine" for
# QtWebEngine. * `{qt_version}`: The underlying Qt version. *
# `{upstream_browser_key}`: "Version" for QtWebKit, "Chrome" for
# QtWebEngine. * `{upstream_browser_version}`: The corresponding
# Safari/Chrome version. * `{qutebrowser_version}`: The currently
# running qutebrowser version.  The default value is equal to the
# unchanged user agent of QtWebKit/QtWebEngine.  Note that the value
# read from JavaScript is always the global value.
# Type: FormatString
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99 Safari/537.36', 'https://*.slack.com/*')

# User agent to send.  The following placeholders are defined:  *
# `{os_info}`: Something like "X11; Linux x86_64". * `{webkit_version}`:
# The underlying WebKit version (set to a fixed value   with
# QtWebEngine). * `{qt_key}`: "Qt" for QtWebKit, "QtWebEngine" for
# QtWebEngine. * `{qt_version}`: The underlying Qt version. *
# `{upstream_browser_key}`: "Version" for QtWebKit, "Chrome" for
# QtWebEngine. * `{upstream_browser_version}`: The corresponding
# Safari/Chrome version. * `{qutebrowser_version}`: The currently
# running qutebrowser version.  The default value is equal to the
# unchanged user agent of QtWebKit/QtWebEngine.  Note that the value
# read from JavaScript is always the global value.
# Type: FormatString
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}; rv:71.0) Gecko/20100101 Firefox/71.0', 'https://docs.google.com/*')

# Load images automatically in web pages.
# Type: Bool
config.set('content.images', True, 'chrome-devtools://*')

# Load images automatically in web pages.
# Type: Bool
config.set('content.images', True, 'devtools://*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'chrome-devtools://*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'devtools://*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'chrome://*/*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'qute://*/*')

# Bindings for normal mode
# config.bind('M', 'hint links spawn mpv --ytdl-format="best[height>=720]+139" {hint-url}')
config.bind('M', 'hint links spawn mpv --script-opts=ytdl_hook-ytdl_path=yt-dlp --ytdl-format="22+139" {hint-url}')
# config.bind('m', 'hint links spawn mpv --ytdl-format="best[height>=480]+139" {hint-url}')
# config.bind('m', 'hint links spawn mpv --ytdl-format="135+139" {hint-url}')
# config.bind('m', 'hint links spawn mpv --script-opts=ytdl_hook-ytdl_path=yt-dlp {hint-url}')
# config.bind('m', 'hint links spawn  yt-dlp {hint-url} -o - | mpv - --force-seekable=yes')
config.bind('m', 'hint links spawn --detach mpv-yt')
# config.bind('ZN', 'hint links spawn alacritty -e yt-dlp -f 22+139 -o "~/vids/qutebrowser/$(date +"%Y-%m-%d_%H-%M-%S")_%(title)s-%(id)s.%(ext)s" {hint-url}')
# config.bind('ZC', 'hint links spawn alacritty -e yt-dlp -f 22+139 -o "~/vids/courses/$(date +"%Y-%m-%d_%H-%M-%S")_%(title)s-%(id)s.%(ext)s" --write-description {hint-url}')
config.bind('zn', 'hint links spawn alacritty -e yt-dlp -f 135+139 -o "~/vids/qutebrowser/$(date +%Y-%m-%d_%H-%M-%S)_%(title)s-%(id)s.%(ext)s" {hint-url}')
# config.bind('zn', 'hint links spawn alacritty -e yt-dlp -f 135+139 -o "~/vids/qutebrowser/$(date +%Y-%m-%d_%H-%M-%S)_%(title)s-%(id)s.%(ext)s" {hint-url}')
# config.bind('zn', 'hint links spawn download_youtube_with_date {hint-url}')
# config.bind('zc', 'hint links spawn alacritty -e yt-dlp -f 135+139 -o "~/vids/courses/$(date +"%Y-%m-%d_%H-%M-%S")_%(title)s-%(id)s.%(ext)s" --write-description {hint-url}')
# config.bind('c', 'hint links yank {url}')
config.bind('c', 'hint links yank {title} {url}')
config.bind('t', 'set-cmd-text -s :open -t')
config.bind('xb', 'config-cycle statusbar.show always never')
config.bind('xt', 'config-cycle tabs.show always never')
config.bind('xx', 'config-cycle statusbar.show always never;; config-cycle tabs.show always never')


# search engines
c.url.searchengines = {'DEFAULT': 'https://duckduckgo.com/?q={}',
        'am': 'https://www.amazon.com/s?k={}',
        'aw': 'https://wiki.archlinux.org/?search={}',
        'goog': 'https://www.google.com/search?q={}',
        're': 'https://www.reddit.com/r/{}',
        'ub': 'https://www.urbandictionary.com/define.php?term={}',
        'wiki': 'https://en.wikipedia.org/wiki/{}',
        'yt': 'https://www.youtube.com/results?search_query={}',
        'alt': 'https://alternativeto.net/browse/search/?q={}',
        'py': 'https://pypi.org/search/?q={}'}


# Bindings for cycling through CSS stylesheets from Solarized Everything CSS:
# https://github.com/alphapapa/solarized-everything-css
config.bind(',ap', 'config-cycle content.user_stylesheets ~/solarized-everything-css/css/apprentice/apprentice-all-sites.css ""')
config.bind(',dr', 'config-cycle content.user_stylesheets ~/solarized-everything-css/css/darculized/darculized-all-sites.css ""')
config.bind(',gr', 'config-cycle content.user_stylesheets ~/solarized-everything-css/css/gruvbox/gruvbox-all-sites.css ""')
config.bind(',sd', 'config-cycle content.user_stylesheets ~/solarized-everything-css/css/solarized-dark/solarized-dark-all-sites.css ""')
config.bind(',sl', 'config-cycle content.user_stylesheets ~/solarized-everything-css/css/solarized-light/solarized-light-all-sites.css ""')

