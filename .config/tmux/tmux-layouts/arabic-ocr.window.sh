# Set window root path. Default is `$session_root`.
# Must be called before `new_window`.
window_root "~/cs/arabic-ocr/"

# Create new window. If no argument is given, window name will be based on
# layout file name.
new_window "arabic-ocr"

# Split window into panes.
# split_h 78
split_v 25

# Run commands.
select_pane 1
run_cmd "source venv/bin/activate"     # runs in active pane
run_cmd "nvim ."     # runs in active pane
select_pane 2
run_cmd "source venv/bin/activate && python3 --version"   # runs in pane 2

select_pane 1


# Paste text
#send_keys "top"    # paste into active pane
#send_keys "date" 1 # paste into pane 1

# Set active pane.
