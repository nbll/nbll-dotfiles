# Set a custom session root path. Default is `$HOME`.
# Must be called before `initialize_session`.
session_root "~/cs/kitabocr"

# Create session with specified name if it does not already exist. If no
# argument is given, session name will be based on layout file name.
# if initialize_session "kitab"; then
initialize_session "kitab"

# Create a new window inline within session layout definition.
#new_window "misc"

# new_window "kitab"
#
# # Split window into panes.
# # split_h 78
# split_v 25
#
# # Run commands.
# select_pane 0
# run_cmd "source venv/bin/activate"     # runs in active pane
# run_cmd "nvim ."     # runs in active pane
# select_pane 1
# run_cmd "source venv/bin/activate && python3 --version"   # runs in pane 2
#
# select_pane 0

# Load a defined window layout.
load_window "kitab"

# Select the default active window on session creation.
#select_window 1

# fi

# Finalize session creation and switch/attach to it.
finalize_and_go_to_session



