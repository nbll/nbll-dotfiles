-- ~/.config/nvim/lua/snippets/python.lua
local ls = require 'luasnip'
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
-- local c = ls.choice_node

return {
  -- snippet for solution code for leetcode
  s('solution', {
    t { 'solution = Solution()', '' },
    t 'result = solution.',
    i(1, 'function_name'),
    t '(',
    i(2, 'args'),
    t { ')', '' },
    t 'print("the result is: ", result)',
  }),
  -- solution = Solution()
  -- result = solution.countConsistentStrings(allowed, words)
  -- print("the number of valid words:", result)

  -- Snippet for for loop
  s('for', {
    t 'for ',
    i(1, 'item'),
    t ' in ',
    i(2, 'iterable'),
    t { ':', '\t' },
    i(3, 'pass'),
  }),
  -- Snippet for print statement
  s('p', {
    t 'print(',
    i(1, '"Hello, World!"'),
    t ')',
  }),

  -- Snippet for function definition
  s('def', {
    t 'def ',
    i(1, 'function_name'),
    t '(',
    i(2, 'args'),
    t { '):', '\t' },
    i(3, 'pass'),
  }),

  -- Snippet for class definition
  s('class', {
    t 'class ',
    i(1, 'ClassName'),
    t '(object):',
    t { '', '\tdef __init__(self):', '\t\t' },
    i(2, 'pass'),
  }),

  -- Snippet for if-else block
  s('ife', {
    t 'if ',
    i(1, 'condition'),
    t { ':', '\t' },
    i(2, 'pass'),
    t { '', 'else:', '\t' },
    i(3, 'pass'),
  }),
}
