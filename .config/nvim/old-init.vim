
if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

filetype plugin indent on       "file type detection, see :help filetype
syntax on                       "syntax highlighting, see :help syntax
set number                      "display line number
set clipboard+=unnamedplus
set path+=**                    "improves searching, see :help path
set noswapfile                  "disable use of swap files
set wildmenu                    "completion menu
set backspace=indent,eol,start  "ensure proper backspace functionality
set nocompatible
" set undodir=~/.cache/nvim/undo  "undo ability will persist after exiting file
set undofile                    "see :help undodir and :help undofile
set incsearch                   "see results while search is being typed, see :help incsearch
set smartindent                 "auto indent on new lines, see :help smartindent
set ic                          "ignore case when searching
" set colorcolumn=80              "display color when line reaches pep8 standards
set expandtab                   "expanding tab to spaces
set tabstop=4                   "setting tab to 4 columns
set shiftwidth=4                "setting tab to 4 columns
set softtabstop=4               "setting tab to 4 columns
set showmatch                   "display matching bracket or parenthesis
set hlsearch incsearch          "highlight all pervious search pattern with incsearch
set hidden
set encoding=utf-8
set bg=light
nnoremap <esc><esc> :noh<return>


highlight ColorColumn ctermbg=8     " display ugly bright red bar at color column number
highlight Search ctermfg=black ctermbg=white
" cterm=NONE

" When python filetype is detected, F5 can be used to execute script
" autocmd FileType python nnoremap <buffer> <F5> :w<cr>:exec '!clear'<cr>:exec '!python3' shellescape(expand('%:p'), 1)<cr>

"vim-plug configuration, plugins will be installed in ~/.config/nvim/plugged
call plug#begin('$XDG_CONFIG_HOME/nvim/plugged')

" Plug 'neovim/nvim-lspconfig'
" Plug 'nvim-lua/completion-nvim'

" Plug 's1n7ax/nvim-terminal'

" Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'
Plug 'preservim/nerdtree'
Plug 'tmhedberg/SimpylFold'
Plug 'vimwiki/vimwiki'
Plug 'dhruvasagar/vim-table-mode'
Plug 'PotatoesMaster/i3-vim-syntax', {'On' : 'i3'}
Plug 'baskerville/vim-sxhkdrc', {'On' : 'sxhkdrc'}
Plug 'ap/vim-css-color'
Plug 'freitass/todo.txt-vim'
Plug 'akinsho/toggleterm.nvim', {'tag' : 'v2.5.0'}

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'bash-lsp/bash-language-server'
" Plug 'itspriddle/vim-shellcheck'

" Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
" Plug 'goballooning/vim-live-latex-preview'

" Plug 'lervag/vimtex'


call plug#end()

" install these after :PlugInstall
" pip3 install -U setuptools pip
" pip3 install python-lsp-server[all]

" neovim LSP Configuration
" lua require('lua_config')

lua require("toggleterm").setup()

" vimwiki settings
let g:vimwiki_list = [{'path': '~/sync/notes/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]

" Enable Tab / Shift Tab to cycle completion options
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
let g:completion_enable_fuzzy_match = 1
set completeopt=menuone,noinsert,noselect

" automatically remove trailing white spaces
autocmd BufWritePre * :%s/\s\+$//e

" SPLIT MANAGEMENT
set splitbelow splitright
" split navigation Ctrl + hjkl
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
" adjusting split sizes
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>

nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTreeToggle<CR>
" nnoremap <C-n> :NERDTree<CR>
" nnoremap <C-f> :NERDTreeFind<CR>

" nnoremap <C-t> :call TermToggle()<CR>
" vnoremap <C-t> <ESC>:call TermToggle()<CR>
" inoremap <C-t> <ESC>:call TermToggle()<CR>
" tnoremap <C-t> exit<CR>

" function! TermToggle()
"     if term_list() == []
"         terminal
"     else
"         for term in term_list()
"             call term_sendkeys(term, "\<cr> exit\<cr>")
"         endfor
"     endif
" endfunction


" let g:livepreview_previewer = 'zathura'
" let g:livepreview_cursorhold_recompile = 0


" :w<CR><CR>
" map <Space><Space> :w<CR><CR> :! pdflatex %<CR><CR>
" map <Space>p :! zathura $(echo % \| sed 's/tex$/pdf/') & disown<CR><CR>













