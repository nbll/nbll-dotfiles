# File originally by Luke Smith <https://lukesmith.xyz>

# This config file will use environmental variables such as $BROWSER and $TERMINAL.
# You can set these variables in ~/.profile or ~/.bash_profile if you have it as below:


# autostart up programs
# exec_always setwall
exec_always compton
exec_always dunst
exec_always sxhkd
exec_always remaps
exec_always redshift -PO 3500
exec_always i3-battery-low
exec_always syncthing -no-browser
exec_always buku-export-to-sync
exec_always feh --bg-scale --randomize $HOME/pics/wallpapers/0*
# exec_always /usr/bin/emacs --daemon
exec_always /snap/bin/emacs --daemon
exec_always ibus restart 2&>1 /dev/null || ibus start

# exec_always ~/.local/bin/personal-repos/activitywatch/aw-qt
# exec_always ~/dls/programs/activitywatch/aw-qt
# exec_always --no-startup-id killall aw-qt & ~/dls/programs/activitywatch/aw-qt
exec_always killall aw-qt || aw-qt
# exec_always aw-qt-once
exec_always birdtray

#exec_always gnome-pomodoro

# Keep the screen from turning off
exec_always xset s off
exec_always xset -dpms
exec_always s noblank


# redshift modes
bindsym $mod+Ctrl+r mode "$mode_redshift"
set $mode_redshift Set colour temperature: (a)uto, (r)eset, (1), (2), (3), (4), (5), (6), (7), (8), (9)
set $kill_redshift pkill -9 redshift;
mode "$mode_redshift" {
    bindsym a exec --no-startup-id "$kill_redshift redshift -P -t 5000:4000", mode "default"
    bindsym r exec --no-startup-id "$kill_redshift redshift -x", mode "default"
    bindsym 1 exec --no-startup-id "$kill_redshift redshift -P -O 1500", mode "default"
    bindsym 2 exec --no-startup-id "$kill_redshift redshift -P -O 2000", mode "default"
    bindsym 3 exec --no-startup-id "$kill_redshift redshift -P -O 2500", mode "default"
    bindsym 4 exec --no-startup-id "$kill_redshift redshift -P -O 3000", mode "default"
    bindsym 5 exec --no-startup-id "$kill_redshift redshift -P -O 3500", mode "default"
    bindsym 6 exec --no-startup-id "$kill_redshift redshift -P -O 4000", mode "default"
    bindsym 7 exec --no-startup-id "$kill_redshift redshift -P -O 4500", mode "default"
    bindsym 8 exec --no-startup-id "$kill_redshift redshift -P -O 5000", mode "default"
    bindsym 9 exec --no-startup-id "$kill_redshift redshift -P -O 5500", mode "default"
    #bindsym 2 exec --no-startup-id "$kill_redshift redshift -P -O 2500", mode "default"

    # exit mode: "Enter" or "Escape"
    bindsym Return mode "default"
    bindsym Escape mode "default"
}


# #---Basic Definitions---# #
set $inner_gaps 7
set $outer_gaps 7
set $mod Mod4
# set $stoprec --no-startup-id dmenurecord kill
set $netrefresh --no-startup-id sudo -A systemctl restart NetworkManager


# set primary gruvbox colorscheme colors
# dark palette
set $dark_bg #282828
set $dark_red #cc241d
set $dark_green #98971a
set $dark_yellow #d79921
set $dark_blue #458588
set $dark_purple #b16286
set $dark_aqua #689d68
set $dark_gray #a89984
set $dark_darkgray #1d2021

set $green_green #00d000

# light palette
set $light_bg #fbf1c7
set $light_red #cc241d
set $light_green #98971a
set $light_yellow #d79921
set $light_blue #458588
set $light_purple #b16286
set $light_aqua #689d68
set $light_gray #7c6f64
set $light_lightgray #f9f5d7

# #---Gaps---# #

for_window [class="^.*"] border pixel 4
gaps inner $inner_gaps
gaps outer $outer_gaps

# default gaps
bindsym $mod+Shift+t		gaps inner current set $inner_gaps; gaps outer current set $outer_gaps
# inner gaps
bindsym $mod+s			gaps inner current plus 5
bindsym $mod+Shift+s		gaps inner current minus 5
# remove gaps
bindsym $mod+Shift+d		gaps inner current set 0; gaps outer current set 0
# outer gaps
bindsym $mod+z			gaps outer current plus 5
bindsym $mod+Shift+z		gaps outer current minus 5


smart_borders on


# bindsym $mod+p [class="vol"] scratchpad show; move position center
# exec_always --no-startup-id pgrep -f '^kitty --class vol,vol' || kitty --class vol,vol -e pulsemixer




#start of window title bars & borders section

# green gruvbox
# class                 border|backgr|text|indicator|child_border
client.focused          $dark_red $dark_green $dark_darkgray $green_green $dark_red
client.unfocused        $dark_blue $dark_darkgray $dark_yellow $green_green $dark_blue
# client.focused_inactive $dark_darkgray $dark_darkgray $dark_yellow $dark_purple $dark_darkgray
# client.urgent           $dark_red $dark_red $dark_white $dark_red $dark_red

# blue gruvbox
# class                 border|backgr|text|indicator|child_border
# client.focused          $blue $blue $darkgray $purple $darkgray
# client.focused_inactive $darkgray $darkgray $yellow $purple $darkgray
# client.unfocused        $darkgray $darkgray $yellow $purple $darkgray
# client.urgent           $red $red $white $red $red

#end of window title bars & borders section


# client.focused          $dark_red $dark_green $dark_darkgray $dark_purple $dark_darkgray

# for python tkinter windows
for_window [class="Tk" instance="tk"] floating enable
for_window [title="Battery Warning"] sticky enable
for_window [class="zatab"] layout tabbed


# #---Dropdown Windows---# #
# General dropdown window traits. The order can matter.
# for_window [instance="dropdown_*"] floating enable
# for_window [instance="dropdown_*"] move scratchpad
# for_window [instance="dropdown_*"] sticky enable
# for_window [instance="dropdown_*"] scratchpad show
# for_window [instance="dropdown_tmuxdd"] resize set 625 450
# for_window [instance="dropdown_dropdowncalc"] resize set 400 300
# for_window [instance="dropdown_tmuxdd"] border pixel 3
# for_window [instance="dropdown_dropdowncalc"] border pixel 2
# for_window [instance="dropdown_*"] move position center

# general for scratchpads
for_window [instance="dropdown_*"] floating enable
for_window [instance="dropdown_*"] move scratchpad
for_window [instance="dropdown_*"] sticky enable
# pulsemixer scratchpad
for_window [instance="dropdown_pulsemixer"] resize set 800 500
for_window [instance="dropdown_pulsemixer"] border pixel 5
bindsym $mod+Shift+p		[class="dropdown_pulsemixer"] scratchpad show; move position center
exec_always --no-startup-id scratchpad-pulsemixer
# ncmpcpp scratchpad
for_window [instance="dropdown_ncmpcpp"] resize set 800 600
for_window [instance="dropdown_ncmpcpp"] border pixel 4
bindsym $mod+Shift+e		[class="dropdown_ncmpcpp"] scratchpad show; move position center
exec_always --no-startup-id scratchpad-ncmpcpp
# lfub scratchpad
for_window [instance="dropdown_lfub"] resize set 800 700
for_window [instance="dropdown_lfub"] border pixel 4
bindsym $mod+Shift+r		[class="dropdown_lfub"] scratchpad show; move position center
exec_always --no-startup-id scratchpad-lfub
# tmux scratchpad
for_window [instance="dropdown_tmux"] resize set 800 650
for_window [instance="dropdown_tmux"] border pixel 4
bindsym $mod+Shift+a		[class="dropdown_tmux"] scratchpad show; move position center
exec_always --no-startup-id scratchpad-tmux

bar {
	font pango:mono 11.4
	status_command i3blocks
	position top
	mode dock
	modifier None
    colors {

        background $dark_bg
        statusline $light_bg
        # workspaces section
        #                    border  backgr. text
        focused_workspace    $dark_aqua $dark_aqua $light_bg
        inactive_workspace   $dark_darkgray $dark_darkgray $light_bg
        active_workspace     $dark_darkgray $dark_darkgray $light_bg
        urgent_workspace     $dark_red $dark_red $dark_bg

        # background $light_bg
        # statusline $dark_bg
        # # workspaces section
        # #                    border  backgr. text
        # focused_workspace    $light_aqua $light_aqua $dark_bg
        # inactive_workspace   $light_lightgray $light_lightgray $dark_bg
        # active_workspace     $light_lightgray $light_lightgray $dark_bg
        # urgent_workspace     $light_red $light_red $light_bg

    }
}

# #---Basic Bindings---# #

bindsym $mod+Escape		workspace prev


#STOP/HIDE EVERYTHING:
bindsym $mod+Shift+Delete	exec --no-startup-id pulsemixer --mute ; exec --no-startup-id mpc pause && pkill -RTMIN+10 i3blocks ; exec --no-startup-id pauseallmpv; workspace 0; exec $term -e htop ; exec $term -e lf

# Show selection:
bindsym $mod+Insert		exec --no-startup-id notify-send "📋 Clipboard contents:" "$(xclip -o -selection clipboard)"
##bindsym $mod+Pause

# #---Letter Key Bindings---# #

# bindsym $mod+Shift+e		exec --no-startup-id tutorialvids

# bindsym $mod+Shift+r		exec --no-startup-id winresize



# bindsym $mod+Shift+y		exec --no-startup-id i3resize left
# bindsym $mod+Shift+u		exec --no-startup-id i3resize down
# bindsym $mod+Shift+i		exec --no-startup-id i3resize up
# bindsym $mod+Shift+o		exec --no-startup-id i3resize right
#---------------------------------------------------------------WINDOW RESIZING

bindsym $mod+Shift+y resize shrink width 1 px or 1 ppt
bindsym $mod+Shift+u resize shrink height 1 px or 1 ppt
bindsym $mod+Shift+i resize grow height 1 px or 1 ppt
bindsym $mod+Shift+o resize grow width 1 px or 1 ppt


# bindsym $mod+p			exec --no-startup-id mpc toggle
# bindsym $mod+Shift+p		exec --no-startup-id mpc pause

# bindsym $mod+a			exec --no-startup-id ddspawn dropdowncalc -f mono:pixelsize=24

set $freeze Distraction-free mode (super+shift+f to reactivate bindings)
mode "$freeze" { bindsym $mod+Shift+f mode "default"
}

bindsym $mod+Shift+f		mode "$freeze" ;; exec --no-startup-id notify-send "Distraction-free mode activated." "Press Super+Shift+f to return."

# bindsym $mod+g			workspace prev
# bindsym $mod+Shift+g		exec --no-startup-id gimp; workspace $ws5


bindsym $mod+x			exec --no-startup-id xset dpms force off && mpc pause && pauseallmpv && slock &
bindsym $mod+Shift+x		exec --no-startup-id prompt "Shutdown computer?" "$shutdown"

#bindsym $mod+c			exec --no-startup-id cabl
# bindsym $mod+Shift+c		exec --no-startup-id camtoggle

#bindsym $mod+Shift+v

bindsym $mod+Shift+b		floating toggle; sticky toggle; exec --no-startup-id hover left

# bindsym $mod+n			exec $term -e newsboat && pkill -RTMIN+6 i3blocks
bindsym $mod+Shift+n		floating toggle; sticky toggle; exec --no-startup-id hover right

# #---Workspace Bindings---# #
bindsym $mod+Home		workspace $ws1
bindsym $mod+Shift+Home		move container to workspace $ws1
bindsym $mod+End		workspace $ws10
bindsym $mod+Shift+End		move container to workspace $ws10
# bindsym $mod+Prior		workspace prev
bindsym $mod+Shift+Prior	move container to workspace prev
# bindsym $mod+Next		workspace next
bindsym $mod+Shift+Next		move container to workspace next
# bindsym $mod+Tab		workspace back_and_forth
# bindsym $mod+XF86Back		workspace prev
bindsym $mod+Shift+XF86Back	move container to workspace prev
# bindsym $mod+XF86Forward	workspace next
bindsym $mod+Shift+XF86Forward	move container to workspace next
# bindsym $mod+semicolon		workspace next
bindsym $mod+apostrophe		split horizontal ;; exec $term
bindsym $mod+slash		split vertical ;; exec $term
bindsym $mod+Shift+slash	kill
# bindsym $mod+backslash		workspace back_and_forth

set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+1		workspace $ws1
bindsym $mod+2		workspace $ws2
bindsym $mod+3		workspace $ws3
bindsym $mod+4		workspace $ws4
bindsym $mod+5		workspace $ws5
bindsym $mod+6		workspace $ws6
bindsym $mod+7		workspace $ws7
bindsym $mod+8		workspace $ws8
bindsym $mod+9		workspace $ws9
bindsym $mod+0		workspace $ws10

# move focused container to workspace
# bindsym $mod+Ctrl+1	    move workspace to workspace $ws1
bindsym $mod+Shift+1	move container to workspace $ws1
bindsym $mod+Shift+2	move container to workspace $ws2
bindsym $mod+Shift+3	move container to workspace $ws3
bindsym $mod+Shift+4	move container to workspace $ws4
bindsym $mod+Shift+5	move container to workspace $ws5
bindsym $mod+Shift+6	move container to workspace $ws6
bindsym $mod+Shift+7	move container to workspace $ws7
bindsym $mod+Shift+8	move container to workspace $ws8
bindsym $mod+Shift+9	move container to workspace $ws9
bindsym $mod+Shift+0	move container to workspace $ws10

for_window [class="Pinentry"] sticky enable
for_window [class="sent"] border pixel 0px
# for_window [title="GIMP Startup"] move workspace $ws5
# for_window [class="Gimp"] move workspace $ws5
# for_window [class="brave-browser"] move workspace $ws1
# for_window [class="keepassxc"] move workspace $ws2
for_window [window_role="GtkFileChooserDialog"] resize set 700 600
for_window [window_role="GtkFileChooserDialog"] move position center
# for_window [title="Default - Wine desktop"] floating enable

# Bindings to make the webcam float and stick.
for_window [title="webcam"] floating enable
for_window [title="webcam"] sticky enable
for_window [title="webcam"] border pixel 0
no_focus [title="webcam"]

# for_window [class="Emacs" instance="emacsclient"] floating move position center, 300, 150 resize set 800, 600

# #---Function Buttons---# #
#bindsym $mod+F1		exec --no-startup-id groff -mom ${XDG_DATA_HOME:-$HOME/.local/share}/larbs/readme.mom -Tpdf | zathura -
#bindsym $mod+Shift+F1	exec --no-startup-id toggle-welcome

# bindsym $mod+F1     exec --no-startup-id sudo light -U 5
# bindsym $mod+Shift+F1     exec --no-startup-id sudo light -A 5

# bindsym $mod+F2		restart
# bindsym $mod+F3		exec --no-startup-id displayselect
bindsym $mod+F4		exec --no-startup-id prompt "Hibernate computer?" "$hibernate"
bindsym $mod+F5		exec --no-startup-id $netrefresh
bindsym $mod+F6		exec --no-startup-id torwrap
bindsym $mod+F7		exec --no-startup-id td-toggle
bindsym $mod+F8		exec --no-startup-id mw sync
# bindsym $mod+F9		exec --no-startup-id dmenumount         ==> sxhkd
# bindsym $mod+F10	exec --no-startup-id dmenuumount             ==> sxhkd
# bindsym $mod+F11	exec --no-startup-id camtoggle              ==> sxhkd
# bindsym $mod+F12	exec $term -e nmtui

# #---Arrow Keys---# #
# bindsym $mod+Left		focus left
# bindsym $mod+Shift+Left		move left
# bindsym $mod+Ctrl+Left		move workspace to output left
# bindsym $mod+Down		focus down
# bindsym $mod+Shift+Down		move down
# bindsym $mod+Ctrl+Down		move workspace to output down
# bindsym $mod+Up			focus up
# bindsym $mod+Shift+Up		move up
# bindsym $mod+Ctrl+Up		move workspace to output up
# bindsym $mod+Right 		focus right
# bindsym $mod+Shift+Right 	move right
# bindsym $mod+Ctrl+Right		move workspace to output right

# #---Media Keys---# #
# Volume keys
#bindsym $mod+m      exec --no-startup-id pulsemixer --toggle-mute && pkill -RTMIN+10 i3blocks
#bindsym $mod+plus		exec --no-startup-id pulsemixer --change-volume +5 && pkill -RTMIN+10 i3blocks
#bindsym $mod+Shift+plus		exec --no-startup-id pulsemixer --change-volume +15 && pkill -RTMIN+10 i3blocks
#bindsym $mod+minus 		exec --no-startup-id pulsemixer --change-volume -5 && pkill -RTMIN+10 i3blocks
#bindsym $mod+Shift+minus	exec --no-startup-id pulsemixer --change-volume -15 && pkill -RTMIN+10 i3blocks
#bindsym $mod+less 		exec --no-startup-id mpc prev
#bindsym $mod+Shift+less		exec --no-startup-id mpc seek 0%
#bindsym $mod+greater		exec --no-startup-id mpc next
#bindsym $mod+Shift+greater	exec --no-startup-id mpc next

# For advancing forward/backward in an mpd song
#bindsym $mod+bracketleft 	exec --no-startup-id mpc seek -10
#bindsym $mod+Shift+bracketleft 	exec --no-startup-id mpc seek -120
#bindsym $mod+bracketright 	exec --no-startup-id mpc seek +10
#bindsym $mod+Shift+bracketright exec --no-startup-id mpc seek +120

# For screenshots and recording
# bindsym Print 			exec --no-startup-id maim ~/Pictures/screenshots/pic-full-"$(date '+%y%m%d-%H%M-%S').png"
# bindsym Shift+Print 		exec --no-startup-id maimpick
# bindsym $mod+Scroll_Lock	exec --no-startup-id "killall screenkey || screenkey"








#start of window title bars & borders section

# green gruvbox
# class                 border|backgr|text|indicator|child_border
# client.focused          $dark_green $dark_green $dark_darkgray $dark_purple $dark_darkgray
# client.focused_inactive $dark_darkgray $dark_darkgray $dark_yellow $dark_purple $dark_darkgray
# client.unfocused        $dark_darkgray $dark_darkgray $dark_yellow $dark_purple $dark_darkgray
# client.urgent           $dark_red $dark_red $dark_white $dark_red $dark_red

# blue gruvbox
# class                 border|backgr|text|indicator|child_border
# client.focused          $blue $blue $darkgray $purple $darkgray
# client.focused_inactive $darkgray $darkgray $yellow $purple $darkgray
# client.unfocused        $darkgray $darkgray $yellow $purple $darkgray
# client.urgent           $red $red $white $red $red

#end of window title bars & borders section





