#+title: doom config
#+author: nbll
#+property: header-args :tangle config.el
#+auto_tangle: t

* themes
#+begin_src emacs-lisp
(use-package doom-themes
  :ensure t
  :config
;; Global settings (defaults)
(setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
      doom-themes-enable-italic t) ; if nil, italics is universally disabled
(load-theme 'doom-gruvbox t)
)
;; Enable flashing mode-line on errors
;;(doom-themes-visual-bell-config)
;; Enable custom neotree theme (all-the-icons must be installed!)
;;(doom-themes-neotree-config)
;; or for treemacs users
;;(setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
;;(doom-themes-treemacs-config)
;; Corrects (and improves) org-mode's native fontification.
;;(doom-themes-org-config)

(use-package all-the-icons
:ensure t)

(set-default 'truncate-lines t)
(map! :leader
      :desc "Clone indirect buffer other window" "b c" #'clone-indirect-buffer-other-window)

;; install jetbrains font mono on linux with
;; /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/JetBrains/JetBrainsMono/master/install_manual.sh)"
(setq doom-font (font-spec :family "JetBrains Mono" :size 14)
      doom-variable-pitch-font (font-spec :family "Ubuntu" :size 14)
      doom-big-font (font-spec :family "JetBrains Mono" :size 22))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))


#+end_src
* org-mode
#+begin_src emacs-lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;              ORG
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(after! org
  (setq org-directory '("~/sync/org/")
        org-agenda-files '("~/sync/org/")))

(after! org
  (setq org-log-done 'time
        org-hide-emphasis-markers t ;; hide asterics when making word bold
        org-table-convert-region-max-lines 20000
        org-todo-keywords        ; This overwrites the default Doom org-todo-keywords
          '((sequence
             ;; "BLOG(b)"           ; Blog writing assignments
             ;; "GYM(g)"            ; Things to accomplish at the gym
             ;; "PROJ(p)"           ; A project that contains other tasks
             ;; "VIDEO(v)"          ; Video assignments
             "TODO(t)"           ; A task that is ready to be tackled
             "DOING(g)"          ; A task is getting worked on
             "WAIT(w)"           ; Something is holding up this task
             "|"                 ; The pipe necessary to separate "active" states and "inactive" states
             "DONE(d)"           ; Task has been completed
             "CANCELLED(c)" ))   ; Task has been cancelled
          ))

(setq   org-highest-priority ?A
    org-default-priority ?B
    org-lowest-priority ?E)

(setq org-refile-targets '((org-agenda-files :maxlevel . 2)))

#+end_src

** org-superstar
#+begin_src emacs-lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;              ORG-SUPERSTAR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package org-superstar
:ensure t
:config
(add-hook 'org-mode-hook (lambda () (org-superstar-mode 1))))

;; (custom-set-faces
;;  '(org-level-1 ((t (:inherit outline-1 :height 1.7))))
;;  '(org-level-2 ((t (:inherit outline-2 :height 1.6))))
;;  '(org-level-3 ((t (:inherit outline-3 :height 1.5))))
;;  '(org-level-4 ((t (:inherit outline-4 :height 1.4))))
;;  '(org-level-5 ((t (:inherit outline-5 :height 1.3))))
;;  '(org-level-6 ((t (:inherit outline-5 :height 1.2))))
;;  '(org-level-7 ((t (:inherit outline-5 :height 1.1)))))

#+end_src

** org-roam
#+begin_src emacs-lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;              ORG-ROAM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package! websocket
    :after org-roam)

(use-package! org-roam-ui
  :after org-roam ;; or :after org
  ;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
  ;;         a hookable mode anymore, you're advised to pick something yourself
  ;;         if you don't care about startup time, use
  ;;  :hook (after-init . org-roam-ui-mode)
  :ensure t
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/sync/roam")
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         ("C-c DEL" . org-mark-ring-goto))

  :config
  (setq org-roam-ui-sync-theme t
        org-roam-ui-follow t
        org-roam-ui-update-on-save t
        org-roam-ui-open-on-start t))
(org-roam-db-autosync-mode)


#+end_src

** org-babel
#+begin_src emacs-lisp
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python     . t)
   (sh         . t)
   (js         . t)
   (emacs-lisp . t)
   (css        . t)
   (plantuml   . t)))

;;
(use-package org-auto-tangle
  ;; :load-path "site-lisp/org-auto-tangle/"    ;; this line is necessary only if you cloned the repo in your site-lisp directory
  :defer t
  :hook (org-mode . org-auto-tangle-mode)
  :config
  (setq org-auto-tangle-default nil))



#+end_src

* evil-surround
#+begin_src emacs-lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;              EVIL-SURROUND
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package evil-surround
  :ensure t
  :config
  (global-evil-surround-mode 1))

#+end_src

#+begin_src emacs-lisp
(setq org-startup-with-inline-images t)


;; (global-set-key (kbd "C-c l") #'org-store-link)


#+end_src

#+begin_src emacs-lisp
;; ====================================
;; ALPHA Transparency
;; ====================================
(set-frame-parameter (selected-frame) 'alpha '(90 . 90))
 (add-to-list 'default-frame-alist '(alpha . (90 . 90)))
;;                                            |    | non focused window
;;                                            | focused widow

#+end_src

* programming
** python
#+begin_src emacs-lisp
;; ====================================
;; Development Setup
;; ====================================

;; (use-package python-mode
;;   :ensure t
;;   :custom
;;   (python-shell-interpreter "python3"))
;;   ;; (add-hook 'python-mode-hook 'lsp-deferred))

;; ;; (after! python
;; ;;   (setq python-shell-display-buffer nil))
;; (after! python
;;   (setq python-shell-completion-native-enable nil
;;         python-shell-interpreter "python"
;;         python-shell-interpreter-args "-i"
;;         python-shell-buffer-name "Python"
;;         python-shell-exec-path (list ".")
;;         python-shell-display-buffer-alist nil))
;; (after! python
;;   (map! :map python-mode-map
;;         ;; Prevent the extra window from opening when executing code
;;         :localleader
;;         :desc "Run Python code" "c" #'python-shell-send-buffer-no-output))


#+end_src

** shell scripting
#+begin_src emacs-lisp

;; ;;; Shell script settings
;; (setq sh-basic-offset 2)
;; (setq sh-indentation 2)

;; ;;; Highlight shell script files
;; (add-to-list 'auto-mode-alist '("\\.sh\\'" . sh-mode))

;; ;;; Smartparens for shell scripts
;; (add-hook 'sh-mode-hook #'smartparens-mode)

;; ;;; Enable flycheck for shell scripts
;; (add-hook 'sh-mode-hook #'flycheck-mode)

;; ;; lsp-mode
;; (use-package lsp-mode
;;   :hook (sh-mode . lsp)
;;   :config
;;   (setq lsp-enable-file-watchers nil) ; Disable file watchers for shell scripts
;;   (setq lsp-shell-shfmt-command "shfmt")) ; Set the shfmt command for formatting

;; ;; Optional: Flycheck for shell scripts
;; (use-package flycheck
;;   :hook (sh-mode . flycheck-mode))

;; ;;; Key bindings for shell scripts
;; (map! :map sh-mode-map
;;       :localleader
;;       :desc "Run shell script" "r" #'shell-command)

#+end_src

** PlantUML
#+begin_src emacs-lisp

;; (setq org-plantuml-jar-path (expand-file-name "~/dls/plantuml-1.2023.13.jar"))
;; (add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
;; (org-babel-do-load-languages 'org-babel-load-languages '((plantuml . t)))

;; (message "The value of jar's path is: %s" org-plantuml-jar-path)


#+end_src

** LaTeX
#+begin_src emacs-lisp

;; (setq lsp-tex-server 'texlab)
;; (setq +latex-viewers '(pdf-tools))

#+end_src

** lsp-mode
#+begin_src emacs-lisp
;; (use-package lsp-mode
;;   :config
;;   ;; (lsp-register-custom-settings
;;   ;;  '(("pyls.plugins.pyls_mypy.enabled" t t)
;;   ;;    ("pyls.plugins.pyls_mypy.live_mode" nil t)
;;   ;;    ("pyls.plugins.pyls_black.enabled" t t)
;;   ;;    ("pyls.plugins.pyls_isort.enabled" t t)))
;;   (setq lsp-auto-guess-root t)
;;   (setq lsp-log-io nil)
;;   (setq lsp-restart 'auto-restart)
;;   (setq lsp-enable-symbol-highlighting nil)
;;   (setq lsp-enable-on-type-formatting nil)
;;   (setq lsp-signature-auto-activate nil)
;;   (setq lsp-signature-render-documentation nil)
;;   ;; (setq lsp-eldoc-hook nil)
;;   (setq lsp-modeline-code-actions-enable nil)
;;   (setq lsp-modeline-diagnostics-enable nil)
;;   (setq lsp-headerline-breadcrumb-enable nil)
;;   (setq lsp-semantic-tokens-enable nil)
;;   (setq lsp-enable-folding nil)
;;   (setq lsp-enable-imenu nil)
;;   (setq lsp-enable-snippet nil)
;;   (setq read-process-output-max (* 1024 1024)) ;; 1MB
;;   (setq lsp-idle-delay 0.5)
;;   :hook
;;   (python-mode . lsp))

;; (use-package company-box
;;  :hook (company-mode . company-box-mode))


#+end_src
** lsp-ui
#+begin_src emacs-lisp
;; (use-package lsp-ui
;;   :config
;;   (setq lsp-ui-sideline-show-hover t
;;         lsp-ui-sideline-delay 0.5
;;         lsp-ui-doc-delay 5
;;         lsp-ui-sideline-ignore-duplicates t
;;         lsp-ui-doc-position 'bottom
;;         lsp-ui-doc-alignment 'frame
;;         lsp-ui-doc-header nil
;;         lsp-ui-doc-include-signature t
;;         lsp-ui-doc-use-childframe t)
;;   :commands lsp-ui-mode)

;; (use-package lsp-pyright
;;   :hook (python-mode . (lambda () (require 'lsp-pyright)))
;;   :init (when (executable-find "python3")
;;           (setq lsp-pyright-python-executable-cmd "python3")))

;; (use-package auto-virtualenv
;;   :ensure t
;;   :init
;;   (use-package pyvenv
;;     :ensure t)
;;   :config
;;   (add-hook 'python-mode-hook 'auto-virtualenv-set-virtualenv)
;;   (add-hook 'projectile-after-switch-project-hook 'auto-virtualenv-set-virtualenv)  ;; If using projectile
;;   )

;; (defun my-python-execute-buffer ()
;;   "Execute the current buffer without opening extra windows."
;;   (interactive)
;;   (let ((display-buffer-alist '(("*Python*" . (display-buffer-no-window)))))
;;     (call-interactively 'py-execute-buffer)))

;; ;; Replace `python-mode-hook` with the actual hook for Python mode
;; (add-hook 'python-mode-hook
;;           (lambda ()
;;             (local-set-key (kbd "C-c C-c") 'my-python-execute-buffer)))


;; (use-package pyvenv
;;   :ensure t
;;   :config
;;   (pyvenv-mode 1))

;; Enable elpy
; (elpy-enable)

;; Enable Flycheck
; (when (require 'flycheck nil t)
;   (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
;   (add-hook 'elpy-mode-hook 'flycheck-mode))


#+end_src
** web-mode
#+begin_src emacs-lisp

;; (after! web-mode
;;   (setq web-mode-markup-indent-offset 4))

;; (setq-hook! 'prettier-js-mode-hook
;;   prettier-js-command "prettier"
;;   prettier-js-args '("--single-quote" "false" "--print-width" "80" "--tab-width" "4" "--use-tabs" "false"))

;; (use-package! prettier-js
;;   :hook ((js2-mode web-mode css-mode) . prettier-js-mode)
;;   :config
;;   (setq prettier-js-args '(
;;                             "--single-quote" "false"
;;                             "--print-width" "80"
;;                             "--tab-width" "4"
;;                             "--use-tabs" "false"
;;                            )))

#+end_src

** editorconfig
#+begin_src emacs-lisp

;; (use-package! editorconfig
;;   :config
;;   (editorconfig-mode 1))


#+end_src
* pdf-tools
#+begin_src emacs-lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;              PDF-TOOLS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (use-package pdf-tools
;;   :defer t
;;   :commands (pdf-loader-install)
;;   :mode "\\.pdf\\'"
;;   ;; :bind (:map pdf-view-mode-map
;;   ;;             ("j" . pdf-view-next-line-or-next-page)
;;   ;;             ("k" . pdf-view-previous-line-or-previous-page)
;;   ;;             ("C-=" . pdf-view-enlarge)
;;   ;;             ("C--" . pdf-view-shrink))
;;   :init (pdf-loader-install)
;;   :config (add-to-list 'revert-without-query ".pdf")
;;   ;; Set the scrolling step and enable pixel-wise scrolling
;;   (setq pdf-view-scroll-step 1
;;         pdf-view-continuous-scroll t))

;; (add-hook 'pdf-view-mode-hook #'(lambda () (interactive) (display-line-numbers-mode -1)))

;; (after! pdf-view
;;   ;; Set scroll step to a smaller value for smoother scrolling
;;   (setq pdf-view-scroll-step 1) ; Adjust the scroll step value as needed

;;   ;; Enable pixel-wise scrolling
;;   (setq pdf-view-continuous-scroll t))


#+end_src
* ediff
#+begin_src emacs-lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;              EDIFF   (built-in)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (setq ediff-split-window-function 'split-window-horizontally
;;       ediff-window-setup-function 'ediff-setup-windows-plain)

;; (defun dt-ediff-hook ()
;;   (ediff-setup-keymap)
;;   (define-key ediff-mode-map "j" 'ediff-next-difference)
;;   (define-key ediff-mode-map "k" 'ediff-previous-difference))

;; (add-hook 'ediff-mode-hook 'dt-ediff-hook)


#+end_src

#+begin_src emacs-lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;              DIRED
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (use-package dired-open
;;   :config
;;   (setq dired-open-extensions '(("gif" . "nsxiv")
;;                                 ("jpg" . "nsxiv")
;;                                 ("png" . "nsxiv")
;;                                 ("mkv" . "mpv")
;;                                 ("mp4" . "mpv"))))

#+end_src

** image-dired
for image previewing in dired
#+begin_src emacs-lisp
;; (tooltip-mode 1)
;; (setq diredp-image-preview-in-tooltip t)
;; (setq diredp-thumbnail-size [120 120])

;; Default values for demo purposes
;; (setq dired-preview-delay 0.7)
;; (setq dired-preview-max-size (expt 2 20))
;; (setq dired-preview-ignored-extensions-regexp
;;       (concat "\\."
;;               "\\(mkv\\|webm\\|mp4\\|mp3\\|ogg\\|m4a"
;;               "\\|gz\\|zst\\|tar\\|xz\\|rar\\|zip"
;;               "\\|iso\\|epub\\|pdf\\)"))

;; ;; Enable `dired-preview-mode' in a given Dired buffer or do it
;; ;; globally:

;; (dired-preview-global-mode 1)

#+end_src
