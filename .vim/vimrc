let mapleader ='\'

" auto install plugins on first vim launch
let data_dir = has('vim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
" Plug 'PotatoesMaster/i3-vim-syntax', {'On' : 'i3'}
Plug 'baskerville/vim-sxhkdrc', {'On' : 'sxhkdrc'}
Plug 'preservim/vim-markdown'
Plug 'itchyny/calendar.vim'
" Plug 'Shougo/deoplete.nvim'
Plug 'itchyny/lightline.vim'
" Plug 'lervag/vimtex'
Plug 'SirVer/ultisnips'   " engine
Plug 'honza/vim-snippets' " snippets
Plug 'vimwiki/vimwiki'

Plug 'davidhalter/jedi-vim'


call plug#end()

filetype plugin on
syntax on
set title hlsearch noruler noshowcmd nocompatible number smartindent expandtab incsearch
set pastetoggle=<F3>
set encoding=utf-8
set bg=light
set go=a
set mouse=a
set clipboard+=unnamedplus
set tabstop=4
set shiftwidth=4
set conceallevel=2


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           jedi-vim
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:jedi#use_tabs_not_buffers = 1
let g:jedi#use_splits_not_buffers = "right"

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           vim-markdown
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:vim_markdown_folding_disabled = 1         " disable folding
" let g:vim_markdown_conceal = 2

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           block cursor
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let &t_SI = "\e[6 q"
let &t_EI = "\e[2 q"

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           lightline
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set laststatus=2    " makes lightline show up
set noshowmode      " hides '-- INSERT --', replaced by lightline

let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ }

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           UltiSnips
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:UltiSnipsExpandTrigger       = '<Tab>'    " use Tab to expand snippets
let g:UltiSnipsJumpForwardTrigger  = '<Tab>'    " use Tab to move forward through tabstops
let g:UltiSnipsJumpBackwardTrigger = '<S-Tab>'  " use Shift-Tab to move backward through tabstops

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           shorten esc latency
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set timeout timeoutlen=1000 ttimeoutlen=5

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           some settings to be renamed
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <esc><esc> :noh<return>
inoremap {<cr> {<cr>}<c-o><s-o>
inoremap [<cr> [<cr>]<c-o><s-o>
inoremap (<cr> (<cr>)<c-o><s-o>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           markdown vimwiki
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:vimwiki_list = [{'path': '~/sync/docs/notes/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           Split management
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set splitbelow splitright
" split navigation Ctrl + hjkl
" nnoremap <C-h> <C-w>h
" nnoremap <C-j> <C-w>j
" nnoremap <C-k> <C-w>k
" nnoremap <C-l> <C-w>l
" adjusting split sizes
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           Term toggle
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! TermToggle()
    if term_list() == []
        terminal
    else
        for term in term_list()
            call term_sendkeys(term, "\<cr> exit\<cr>")
        endfor
    endif
endfunction

nnoremap <C-t> :call TermToggle()<CR>
vnoremap <C-t> <ESC>:call TermToggle()<CR>
inoremap <C-t> <ESC>:call TermToggle()<CR>
tnoremap <C-t> exit<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           remove trailing white spaces
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd BufWritePre * :%s/\s\+$//e

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           Toggle quickfix
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! ToggleQuickFix()
    if empty(filter(getwininfo(), 'v:val.quickfix'))
        copen
    else
        cclose
    endif
endfunction

nnoremap <Space>q :call ToggleQuickFix()<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           Colors
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
hi Search cterm=NONE ctermfg=black ctermbg=white


















