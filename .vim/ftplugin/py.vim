vim9script


compiler py

def RunPython()
    w
    cd %:p:h
    silent make
    cd -
    cwindow # opens only on error
    redraw!
enddef


nnoremap <buffer> <Space><Space> <ScriptCmd>RunPython()<LF>

nnoremap <buffer> <Space>s :so /home/nbl/.vim/ftplugin/py.vim<LF>
