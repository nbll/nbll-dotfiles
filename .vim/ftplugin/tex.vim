vim9script


compiler tex

def Compile()
    w
    cd %:p:h
    silent make
    cd -
    cwindow
    redraw!
enddef

def OnError(channelname: channel, msg: string)
    echoerr "processus has finished with error: " .. msg
enddef

def Openpdf()
    # silent !zathura '%:p:r'.pdf & disown
    # const opta = "--synctex-forward "
    # const optb = line(".") .. ":" .. col(".") .. ":" .. '%:p'
    # const optc = "-c ~/.config/zathura/normal-theme "
    # add optc before opta for normal zathura theme
    # exec "silent !zathura '%:p:r'.pdf " .. opta .. optb .. "& disown"
    # redraw!
    const proc = "zathura"
    const opta = "-c"
    const optb = "~/.config/zathura/normal-theme"
    const optc = expand('%:p:r') .. ".pdf"
    const optd = "--synctex-forward"
    const opte = line(".") .. ":" .. col(".") .. ":" .. expand('%:p')
    const cmd  = [proc, optc, optd, opte]
    job_start(cmd, {"err_cb": OnError})
enddef

nnoremap <buffer> <Space><Space> <ScriptCmd>Compile()<LF>
nnoremap <buffer> <Space>r <ScriptCmd>Openpdf()<LF>


nnoremap <buffer> <Space>s :so /home/nabilpc/.vim/ftplugin/tex.vim<LF>

