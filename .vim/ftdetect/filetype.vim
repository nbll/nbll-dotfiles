vim9script

au BufNewFile,BufRead *.c set filetype=c
au BufNewFile,BufRead *.sh set filetype=sh
au BufNewFile,BufRead *.vim set filetype=vim
au BufNewFile,BufRead *.tex set filetype=tex
# au BufNewFile,BufRead *.py set filetype=py
# au BufNewFile,BufRead *.md set filetype=md
