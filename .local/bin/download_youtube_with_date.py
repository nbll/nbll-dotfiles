
from os import system
from time import strftime, localtime

# Function to download YouTube video with upload date in filename
def download_youtube_with_date(url):
    # Get the current time
    current_time = strftime("%Y-%m-%d_%H-%M-%S", localtime())

    # Use youtube-dl command to download the video with upload date in the filename
    system(f'youtube-dl -o "~/Downloads/%(upload_date)s_{current_time}.%(ext)s" {url}')
    system(f'yt-dlp -f 135+139 -o "~/vids/qutebrowser/{current_time}_%(title)s-%(id)s.%(ext)s" {url}')
    # config.bind('zn', 'hint links spawn alacritty -e yt-dlp -f 135+139 -o "~/vids/qutebrowser/$(date +"%Y-%m-%d_%H-%M-%S")_%(title)s-%(id)s.%(ext)s" {hint-url}')

# Create a Qutebrowser binding for the custom command
# config.bind('<Ctrl-Shift-d>', 'hint links spawn --detach download_youtube_with_date {hint-url}')

