#
# ~/.bashrc
#

PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

# If not running interactively, do not do anything
[[ $- != *i* ]] && return
# Otherwise start tmux
[[ -z "$TMUX" ]] && exec tmux -S "$HOME/my-tmux-socket"

HISTSIZE=1000000
HISTFILE=~/.bash_history
# HISTFILE="${XDG_CACHE_HOME:-$HOME/.cache}/bash/history"

# vim mode in the terminal
set -o vi

# change shape of vim cursor (doesn't work on bash ??)
# set show-mode-in-prompt on
# set vi-cmd-mode-string "\1\e[2 q\2"
# set vi-ins-mode-string "\1\e[6 q\2"

export TMUXIFIER_LAYOUT_PATH="$HOME/.config/tmux/tmux-layouts"
export PATH="$PATH:/opt/nvim-linux64/bin"
export PATH="$PATH:$HOME/.config/tmux/plugins/tmuxifier/bin"
export PATH="$HOME/.config/emacs/bin:$PATH"
export PATH="$HOME/.npm-global/bin:$PATH"
export PATH="$PATH:/mnt/c/Users/nabil/code/scripts/win32yank"
DOOMDIR="$HOME/.config/doom"
EMACSDIR="$HOME/.config/emacs/"

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

#ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

## Personal aliases and functions
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliases" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliases"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/functions" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/functions"

#shopt
shopt -s autocd # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend # do not overwrite history
shopt -s expand_aliases # expand aliases

# This is for accessing the history from all the terminal windows
# Avoid duplicates
HISTCONTROL=ignoredups:erasedups
# After each command, append to the history file and reread it
PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a; history -c; history -r"

# reporting tools - install when not installed
# neofetch
# cpufetch

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# zsh like completion
# https://superuser.com/questions/288714/bash-autocomplete-like-zsh
# bind 'set show-all-if-ambiguous on'
# bind 'TAB:menu-complete'
# bind '"\e[Z":menu-complete-backward'
[ -f /home/nabilpc/.local/builds/programs/alacritty/extra/completions/alacritty.bash ] && source /home/nabilpc/.local/builds/programs/alacritty/extra/completions/alacritty.bash



"$HOME/.local/bin/ufetch"



# Check environment and define a paste function
if uname -r | grep -qi microsoft; then
  # WSL: Use win32yank for clipboard
  paste_clipboard() {
    echo -n "$(win32yank.exe -o)"
  }
else
  # Ubuntu: Use xclip for clipboard
  paste_clipboard() {
    echo -n "$(xclip -selection clipboard -o)"
  }
fi

# Bind Ctrl+P to execute paste_clipboard function and insert its output
bind -x '"\C-p": "READLINE_LINE=$(paste_clipboard); READLINE_POINT=${#READLINE_LINE}"'
bind -x '"\C-f":"tmux-sessionizer"'


# fnm
FNM_PATH="/home/nabil/.local/share/fnm"
if [ -d "$FNM_PATH" ]; then
  export PATH="$FNM_PATH:$PATH"
  eval "`fnm env`"
fi


# Load Angular CLI autocompletion.
source <(ng completion script)
