# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# add cargo binaries to the path
if [ -d "$HOME/.local/share/cargo/bin" ] ;
  then PATH="$HOME/.local/share/cargo/bin:$PATH"
fi

# add cargo binaries to the path
if [ -d "$HOME/.cargo/bin" ] ;
  then PATH="$HOME/.cargo/bin:$PATH"
fi

if [ -d "/usr/local/go/bin" ] ;
  then PATH="/usr/local/go/bin:$PATH"
fi
# export PATH=$PATH:/usr/local/go/bin
# add dm-scripts from dt to the path
if [ -d "$HOME/.local/bin/dmscripts/scripts" ] ;
  then PATH="$HOME/.local/bin/dmscripts/scripts:$PATH"
fi

# add emacsclient to path
if [ -d "$HOME/.config/emacs/bin" ] ;
  then PATH="$HOME/.config/emacs/bin:$PATH"
fi

# export PYENV_ROOT="$HOME/.pyenv"
# [[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
# eval "$(pyenv init -)"


export TERMINAL="alacritty"
export BROWSER="brave-browser"
# export BROWSER="firefox-research"
export EDITOR="nvim"
export VISUAL="nvim"
export PAGER="less"

export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"

export ANDROID_HOME="$XDG_DATA_HOME"/android
# export HISTFILE="${XDG_DATA_HOME}"/bash/history
export CARGO_HOME="$XDG_DATA_HOME"/cargo
# export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history
export NBRC_PATH="$XDG_CONFIG_HOME/nbrc"
export NODE_REPL_HISTORY="$XDG_DATA_HOME"/node_repl_history
# export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export GOPATH="$HOME/.local/builds/programs/go-programs"
export PYTHONSTARTUP="${XDG_CONFIG_HOME}/python/pythonrc"
export SQLITE_HISTORY="$XDG_CACHE_HOME"/sqlite_history
export LF_BOOKMARK_PATH="$HOME"/sync/books/bookmarks/lf-bookmarks
export QT_QPA_PLATFORMTHEME=qt5ct

# this is adding pyenv to path
# export PYENV_ROOT="$HOME/.pyenv"
# command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
# eval "$(pyenv init -)"

# export LF_ICONS="di=📁:\
# fi=📃:\
# tw=🤝:\
# ow=📂:\
# ln=⛓:\
# or=❌:\
# ex=🎯:\
# *.txt=✍:\
# *.mom=✍:\
# *.me=✍:\
# *.ms=✍:\
# *.png=🖼:\
# *.webp=🖼:\
# *.ico=🖼:\
# *.jpg=📸:\
# *.jpe=📸:\
# *.jpeg=📸:\
# *.gif=🖼:\
# *.svg=🗺:\
# *.tif=🖼:\
# *.tiff=🖼:\
# *.xcf=🖌:\
# *.html=🌎:\
# *.xml=📰謹:\
# *.gpg=🔒:\
# *.css=🎨:\
# *.pdf=📚:\
# *.djvu=📚:\
# *.epub=📚:\
# *.csv=📓:\
# *.xlsx=📓:\
# *.tex=📜:\
# *.md=📘:\
# *.r=📊:\
# *.R=📊:\
# *.rmd=📊:\
# *.Rmd=📊:\
# *.m=📊:\
# *.mp3=🎵:\
# *.opus=🎵:\
# *.ogg=🎵:\
# *.m4a=🎵:\
# *.flac=🎼:\
# *.wav=🎼:\
# *.mkv=🎥:\
# *.mp4=🎥:\
# *.webm=🎥:\
# *.mpeg=🎥:\
# *.avi=🎥:\
# *.mov=🎥:\
# *.mpg=🎥:\
# *.wmv=🎥:\
# *.m4b=🎥:\
# *.flv=🎥:\
# *.zip=📦:\
# *.rar=📦:\
# *.7z=📦:\
# *.tar.gz=📦:\
# *.z64=🎮:\
# *.v64=🎮:\
# *.n64=🎮:\
# *.gba=🎮:\
# *.nes=🎮:\
# *.gdi=🎮:\
# *.1=ℹ:\
# *.nfo=ℹ:\
# *.info=ℹ:\
# *.log=📙:\
# *.iso=📀:\
# *.img=📀:\
# *.bib=🎓:\
# *.ged=👪:\
# *.part=💔:\
# *.torrent=🔽:\
# *.jar=♨:\
# *.java=♨:\
# "

# lf icons
# export LF_ICONS="\
# tw=:\
# st=:\
# ow=:\
# dt=:\
# di=:\
# fi=:\
# ln=:\
# or=:\
# ex=:\
# *.c=:\
# *.cc=:\
# *.clj=:\
# *.coffee=:\
# *.cpp=:\
# *.css=:\
# *.d=:\
# *.dart=:\
# *.erl=:\
# *.exs=:\
# *.fs=:\
# *.go=:\
# *.h=:\
# *.hh=:\
# *.hpp=:\
# *.hs=:\
# *.html=:\
# *.java=:\
# *.jl=:\
# *.js=:\
# *.json=:\
# *.lua=:\
# *.md=:\
# *.php=:\
# *.pl=:\
# *.pro=:\
# *.py=:\
# *.rb=:\
# *.rs=:\
# *.scala=:\
# *.ts=:\
# *.vim=:\
# *.cmd=:\
# *.ps1=:\
# *.sh=:\
# *.bash=:\
# *.zsh=:\
# *.fish=:\
# *.tar=:\
# *.tgz=:\
# *.arc=:\
# *.arj=:\
# *.taz=:\
# *.lha=:\
# *.lz4=:\
# *.lzh=:\
# *.lzma=:\
# *.tlz=:\
# *.txz=:\
# *.tzo=:\
# *.t7z=:\
# *.zip=:\
# *.z=:\
# *.dz=:\
# *.gz=:\
# *.lrz=:\
# *.lz=:\
# *.lzo=:\
# *.xz=:\
# *.zst=:\
# *.tzst=:\
# *.bz2=:\
# *.bz=:\
# *.tbz=:\
# *.tbz2=:\
# *.tz=:\
# *.deb=:\
# *.rpm=:\
# *.jar=:\
# *.war=:\
# *.ear=:\
# *.sar=:\
# *.rar=:\
# *.alz=:\
# *.ace=:\
# *.zoo=:\
# *.cpio=:\
# *.7z=:\
# *.rz=:\
# *.cab=:\
# *.wim=:\
# *.swm=:\
# *.dwm=:\
# *.esd=:\
# *.jpg=:\
# *.jpeg=:\
# *.mjpg=:\
# *.mjpeg=:\
# *.gif=:\
# *.bmp=:\
# *.pbm=:\
# *.pgm=:\
# *.ppm=:\
# *.tga=:\
# *.xbm=:\
# *.xpm=:\
# *.tif=:\
# *.tiff=:\
# *.png=:\
# *.svg=:\
# *.svgz=:\
# *.mng=:\
# *.pcx=:\
# *.mov=:\
# *.mpg=:\
# *.mpeg=:\
# *.m2v=:\
# *.mkv=:\
# *.webm=:\
# *.ogm=:\
# *.mp4=:\
# *.m4v=:\
# *.mp4v=:\
# *.vob=:\
# *.qt=:\
# *.nuv=:\
# *.wmv=:\
# *.asf=:\
# *.rm=:\
# *.rmvb=:\
# *.flc=:\
# *.avi=:\
# *.fli=:\
# *.flv=:\
# *.gl=:\
# *.dl=:\
# *.xcf=:\
# *.xwd=:\
# *.yuv=:\
# *.cgm=:\
# *.emf=:\
# *.ogv=:\
# *.ogx=:\
# *.aac=:\
# *.au=:\
# *.flac=:\
# *.m4a=:\
# *.mid=:\
# *.midi=:\
# *.mka=:\
# *.mp3=:\
# *.mpc=:\
# *.ogg=:\
# *.ra=:\
# *.wav=:\
# *.oga=:\
# *.opus=:\
# *.spx=:\
# *.xspf=:\
# *.pdf=:\
# *.nix=:\
# "
# 辶

